#include <iostream>
using namespace std;

class DoubleHashArray{
public:
    void add(int i);
    int get(int i);
    int remove(int i);
    void display();

    DoubleHashArray(int size){
        if(isPrime(size)){
            N = size;
            array = new int[N];
            for(int i = 0; i<N; ++i){
                array[i] = NULL;
            }
            q = N-1;
            while(!isPrime(q)){
                q--;
            }
        }else{
            cout<<"Blad, rozmiar nie jest liczba pierwsza\n";
        }
    }

private:
    int N;
    int q;
    int* array;
    ~DoubleHashArray(){}

    bool isPrime(int n){
        for(int i=2; i<n; i++){
            if(n % i == 0){
                return false;
            }
        }
        return true;
    }

    int hash2(int i){
        return q - i%q;
    }

    int hash(int i){
        return i%N;
    }
};

void DoubleHashArray::display(){
    for(int i = 0; i<N; ++i){
        cout<<array[i]<<"\n";
    }
}

int DoubleHashArray::get(int i){
    for(int k = 0; k<N; ++k){
        if(array[k] == i){
            cout<<"Przeszukano "<<k+1<<"probek \n";
            return k;
        }
    }
    return NULL;
}

int DoubleHashArray::remove(int i){
    for(int k = 0; k<N; k++){
        if(array[k] == i){
            array[k] = NULL;
            cout<<"Przeszukano "<<k+1<<"probek \n";
            return k;
        }
    }
    return NULL;
}

void DoubleHashArray::add(int i){
    int key = hash(i);
    int licznik = 1;
    if(array[key] == NULL){
        array[key] = i;
    }else{
        int j = 1;
        int key2 = hash(key + j*hash2(i));
        while(array[key2] != NULL){
             licznik++;
             j++;
             key2 = hash(key + j*hash2(i));
        }
        array[key2] = i;
    }
    cout<<"Potrzebne bylo "<<licznik<<"probek \n";
}
