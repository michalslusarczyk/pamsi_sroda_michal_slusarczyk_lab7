#include <iostream>
#include <cstdlib>
#include <ctime>
#include "LinkedHashArray.cpp"
#include "DoubleHashArray.cpp"
#include "LineHashArray.cpp"

#define N 7
using namespace std;

int main(){

    //srand(time(NULL));

    DoubleHashArray* array = new DoubleHashArray(N);

    for(int i=0; i<N; ++i){
        array->add(rand());
    }
    cout<<"------------------------------\n";
    array->display();
    cout<<"------------------------------\n";
    cout<<"Wybierz klucz do usuniecia:\n";
    int b;
    cin>>b;
    array->remove(b);
    cout<<"------------------------------\n";
    array->display();

}




