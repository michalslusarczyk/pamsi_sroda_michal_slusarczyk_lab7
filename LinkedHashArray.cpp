#include <iostream>
#include <cstdlib>
#include "List.cpp"
using namespace std;

class LinkedHashArray{
public:
    void add(int i);
    int get(int i);
    int remove(int i);
    void display();

    LinkedHashArray(int size){
        if(isPrime(size)){
            N = size;
            array = new List<int>*[N];
            for(int i = 0; i<N; ++i){
                List<int>* temp = new List<int>();
                array[i] = temp;
            }
        }else{
            cout<<"Blad, rozmiar nie jest liczba pierwsza\n";
        }
    }

private:
    int N;
    List<int>** array;
    ~LinkedHashArray(){}

    bool isPrime(int n){
        for(int i=2; i<n; i++){
            if(n % i == 0){
                return false;
            }
        }
        return true;
    }

    int hash(int i){
        return i%N;
    }
};

int LinkedHashArray::get(int i){
    int licznik = 0;
    for(int k = 0; k<N; ++k){
        ListElement<int>* current = array[k]->getFirst();
        while(current != NULL){
                licznik++;
            if(current->getElement() == i){
                cout<<"Przeszukano "<<licznik<<"probek \n";
                return k;
            }
            current = current->getNext();
        }
    }
    cout<<"Przeszukano "<<licznik<<"probek\n";
    return NULL;
}

void LinkedHashArray::display(){
    for(int i=0; i<N; ++i){
        cout<<"Tablica nr "<<i<<"\n";
        array[i]->display();
        cout<<"=========\n";
    }
}

int LinkedHashArray::remove(int i){
    int licznik = 0;
    int found = -1;
    for(int k = 0; k<N; ++k){
        List<int>* tmp = new List<int>();
        List<int>* current = array[k];
        while(!current->isEmpty()){
            int s = current->removeFront()->getElement();
            licznik++;
            if(s != i){
                tmp->addFront(s);
            }else{
                found = k;
                cout<<"Przeszukano "<<licznik<<"probek \n";
            }

        }
        while(!tmp->isEmpty())
            current->addFront(tmp->removeFront()->getElement());

            if(found != -1)
                return k;
    }

    return found;
}

void LinkedHashArray::add(int i){
    int key = hash(i);
    array[key]->addFront(i);
}

