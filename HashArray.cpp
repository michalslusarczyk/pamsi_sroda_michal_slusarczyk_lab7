#include <iostream>
using namespace std;

class HashArray{
public:
    void add(int i);
    int get(int i);
    int remove(int i);
    void display();

    HashArray(int size){
        if(isPrime(size)){
            N = size;
            array = new int[N];
            for(int i = 0; i<N; ++i){
                array[i] = NULL;
            }
            q = N-1;
            while(!isPrime(q)){
                q--;
            }
        }else{
            cout<<"Blad, rozmiar nie jest liczba pierwsza\n";
        }
    }

private:
    int N;
    int q;
    int* array;
    ~HashArray(){}

    bool isPrime(int n){
        for(int i=2; i<n; i++){
            if(n % i == 0){
                return false;
            }
        }
        return true;
    }

    int hash2(int i){
        return q - i%q;
    }

    int hash(int i){
        return i%N;
    }
};

void HashArray::display(){
    for(int i = 0; i<N; ++i){
        cout<<array[i]<<"\n";
    }
}

int HashArray::get(int i){
    for(int k = 0; k<N; ++k){
        if(array[k] == i)
            return k;
    }
    return NULL;
}

int HashArray::remove(int i){
    for(int k = 0; k<N; ++k){
        if(array[k] == i)
            array[k] = NULL;
            return k;
    }
    return NULL;
}

void HashArray::add(int i){
    int key = hash(i);
    if(array[key] == NULL){
        array[key] = i;
    }else{
        int j = 1;
        int key2 = hash(key + j*hash2(i));
        while(array[key2] != NULL){
             j++;
             key2 = hash(key + j*hash2(i));
        }
        array[key2] = i;
    }
}
