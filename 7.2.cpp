#include <iostream>
#include "AVLTree.cpp"
#include <cstdlib>

#define N 23



int main(){

    AVLTree* tree = new AVLTree();
    tree->add(2);
    tree->add(24);
    tree->add(27);
    tree->add(1);
    tree->add(5);
    tree->add(10);
    tree->add(56);
    tree->add(89);
    tree->add(87);
    tree->add(120);
    tree->add(7);
    tree->add(8);
    tree->add(66);
    tree->add(465);
    tree->add(3);
    tree->add(99);

    cout<<"\nTree height: "<<tree->getHeight(tree->getRoot())<<"\n";
    tree->display();
    return 0;
}
