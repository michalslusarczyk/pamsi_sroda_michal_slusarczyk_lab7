#include <iostream>
using namespace std;

class BinaryTreeNode{
public:
    BinaryTreeNode* getParent(){
        return parent;
    }
    BinaryTreeNode* getLeft(){
        return left;
    }
    BinaryTreeNode* getRight(){
        return right;
    }

    int getKey(){
        return key;
    }
    int getBalanceFactor(){
        return balance;
    }

    void setParent(BinaryTreeNode* node){
        parent = node;
    }
    void setLeft(BinaryTreeNode* node){
        left = node;
    }
    void setRight(BinaryTreeNode* node){
        right = node;
    }

    void setKey(int key){
        this->key = key;
    }
    void setBalanceFactor(int balance){
        this->balance = balance;
    }

    bool isExternal(){
        return left == NULL && right == NULL;
    }

    bool isRoot(){
        return parent == NULL;
    }
private:
    BinaryTreeNode* parent;
    BinaryTreeNode* left;
    BinaryTreeNode* right;

    int key;
    int balance;
};

class AVLTree{
public:
    void add(int key);
    void remove(int key);
    void display();

    int getHeight(BinaryTreeNode* node);

    int getRootKey(){
        return root->getKey();
    }

    BinaryTreeNode* getRoot(){
        return root;
    }
private:
    BinaryTreeNode* root;

    void rotationRight(BinaryTreeNode* node);
    void rotationLeft(BinaryTreeNode* node);
    void rotationRightLeft(BinaryTreeNode* node);
    void rotationLeftRight(BinaryTreeNode* node);

    BinaryTreeNode* removeNode(BinaryTreeNode* node);
    BinaryTreeNode* findPreNode(BinaryTreeNode* node);
    BinaryTreeNode* findNode(int key);

    void print(BinaryTreeNode* one, BinaryTreeNode* two, string komunikat);
};

void AVLTree::print(BinaryTreeNode* one, BinaryTreeNode* two, string komunikat){
    cout<<"[Wezel: ";
    if(one != NULL)
        cout<<one->getKey()<<"] ";
    else
        cout<<"NULL] ";

    cout<<komunikat<<" ";

    if(two != NULL)
        cout<<two->getKey()<<"\n";
    else
        cout<<"NULL \n";

}

void AVLTree::display(){
    cout<<"\tROOT: ["<<getRootKey()<<"]\n";
    cout<<"L:[";
    if(getRoot()->getLeft() != NULL){
        cout<<getRoot()->getLeft()->getKey()<<"]";
    }else{
        cout<<"[NULL]";
    }

    if(getRoot()->getRight() != NULL){
        cout<<"\t\tR:["<<getRoot()->getRight()->getKey();
    }else{
        cout<<"[NULL";
    }
    cout<<"]\n";
    int hL = getHeight(getRoot()->getLeft());
    int hR = getHeight(getRoot()->getRight());
    cout<<"H:"<<hL<<"\t\tH:"<<hR<<"\n";
    cout<<"Difference: "<<hL-hR<<"\n";
}

int AVLTree::getHeight(BinaryTreeNode* node){
    if(node->isExternal() || node == NULL){
        return 0;
    }else{
        int h = 0;
        if(node->getLeft() != NULL)
            h = std::max(h, getHeight(node->getLeft()));
        if(node->getRight() != NULL)
            h = std::max(h, getHeight(node->getRight()));
        return h+1;
    }
}

BinaryTreeNode* AVLTree::findNode(int key){
    BinaryTreeNode* temp = root;
    while(temp != NULL && temp->getKey() != key){
        if(key<temp->getKey()){
            temp = temp->getLeft();
        }else{
            temp = temp->getRight();
        }
    }
    return temp;
}

BinaryTreeNode* AVLTree::findPreNode(BinaryTreeNode* node){
    BinaryTreeNode* temp;
    if(node != NULL){
        if(node->getLeft() != NULL){
            node = node->getLeft();
            while(node->getRight() != NULL){
                node = node->getRight();
            }
        }else{
            do{
                temp = node;
                node = node->getParent();
            }while(node != NULL && node->getRight() != temp);
        }
    }
    return node;
}

BinaryTreeNode* AVLTree::removeNode(BinaryTreeNode* node){

    BinaryTreeNode* temp;
    BinaryTreeNode* temp2;
    BinaryTreeNode* temp3;

    bool nest;

    if(node->getLeft() != NULL && node->getRight() != NULL){
        temp2 = removeNode(findPreNode(node));
        nest = false;
    }else{
        if(node->getLeft() != NULL){
            temp2 = node->getLeft();
            node->setLeft(NULL);
        }else{
            temp2 = node->getRight();
            node->setRight(NULL);
        }

        node->setBalanceFactor(0);
        nest = true;
    }
    if(temp2 != NULL){
        temp2->setParent(node->getParent());
        temp2->setLeft(node->getLeft());
        if(temp2->getLeft() != NULL){
            temp2->getLeft()->setParent(temp2);
        }
        temp2->setRight(node->getRight());
        if(temp2->getRight() != NULL){
            temp2->getRight()->setParent(temp2);
        }
        temp2->setBalanceFactor(node->getBalanceFactor());
    }
    if(node->getParent() != NULL){
        if(node->getParent()->getLeft() == node){
            node->getParent()->setLeft(temp2);
        }else{
            node->getParent()->setRight(temp2);
        }
    }else{
        this->root = temp2;
    }
    if(nest){
        temp3 = temp2;
        temp2 = node->getParent();
        while(temp2 != NULL){
            if(temp2->getBalanceFactor() == 0){
                if(temp2->getLeft() == temp3){
                    temp2->setBalanceFactor(-1);
                }else{
                    temp2->setBalanceFactor(1);
                }
                break;
            }else{
                if((temp2->getBalanceFactor() == 1 && temp2->getLeft() == temp3) || (temp2->getBalanceFactor() == -1 && temp2->getRight() == temp3)){
                    temp2->setBalanceFactor(0);
                    temp3 = temp2;
                    temp2 = temp2->getParent();
                }else{
                    if(temp2->getLeft() == temp3){
                        temp = temp2->getRight();
                    }else{
                        temp = temp2->getLeft();
                    }
                    if(temp->getBalanceFactor() == 0){
                        if(temp2->getBalanceFactor() == 1){
                            rotationLeft(temp2);
                        }else{
                            rotationRight(temp2);
                        }
                        break;
                    }else if(temp2->getBalanceFactor() == temp->getBalanceFactor()){
                        if(temp2->getBalanceFactor() == 1){
                            rotationLeft(temp2);
                        }else{
                            rotationRight(temp2);
                        }
                        temp3 = temp;
                        temp2 = temp->getParent();
                    }else{
                        if(temp2->getBalanceFactor() == 1){
                            rotationLeftRight(temp2);
                        }else{
                            rotationRightLeft(temp2);
                        }
                        temp3 = temp2->getParent();
                        temp2 = temp3->getParent();
                    }
                }
            }
        }
    }

    return node;
}

void AVLTree::remove(int key){
    removeNode(findNode(key));
}

void AVLTree::add(int key){

    BinaryTreeNode* next = new BinaryTreeNode();
    next->setKey(key);
    next->setLeft(NULL);
    next->setRight(NULL);
    next->setBalanceFactor(0);

    BinaryTreeNode* currentNode = root;
    if(root == NULL){
        next->setParent(NULL);
        root = next;
    }else{
        bool placedCorrectly = false;
        while(!placedCorrectly){
            if(next->getKey() < currentNode->getKey()){
                if(currentNode->getLeft() == NULL){
                    next->setParent(currentNode);
                    currentNode->setLeft(next);
                    placedCorrectly = true;
                }else{
                    currentNode = currentNode->getLeft();
                }
            }else{
                if(currentNode->getRight() == NULL){
                    next->setParent(currentNode);
                    currentNode->setRight(next);
                    placedCorrectly = true;
                }else{
                    currentNode = currentNode->getRight();
                }
            }
        }
        //BALANS
        if(currentNode->getBalanceFactor() != 0){
            currentNode->setBalanceFactor(0);
        }else{
            if(currentNode->getLeft() == next){
                currentNode->setBalanceFactor(1);
            }else{
                currentNode->setBalanceFactor(-1);
            }

            BinaryTreeNode* temp = currentNode->getParent();
            bool unbalanced = false;
            while(temp != NULL){
                if(temp->getBalanceFactor() != 0){
                    unbalanced = true;
                    break;
                }else{
                    if(temp->getLeft() == currentNode){
                        temp->setBalanceFactor(1);
                    }else{
                        temp->setBalanceFactor(-1);
                    }

                    currentNode = temp;
                    temp = temp->getParent();
                }
            }

            if(unbalanced){
                if(temp->getBalanceFactor() == 1){
                    if(temp->getRight() == currentNode){
                        temp->setBalanceFactor(0);
                    }else if(currentNode->getBalanceFactor() == -1){
                        rotationLeftRight(temp);
                    }else{
                        rotationLeft(temp);
                    }
                }else{
                    if(temp->getLeft() == currentNode){
                        temp->setBalanceFactor(0);
                    }else if(currentNode->getBalanceFactor() == 1){
                        rotationRightLeft(temp);
                    }else{
                        rotationRight(temp);
                    }
                }
            }
        }
    }
}

void AVLTree::rotationRight(BinaryTreeNode* node){
    BinaryTreeNode* temp = node->getRight();
    BinaryTreeNode* temp2 = node->getParent();


    print(node, temp->getLeft(), "prawym synem staje sie");
    node->setRight(temp->getLeft());
    if(node->getRight() != NULL){
        node->getRight()->setParent(node);
    }

    print(temp, node, "lewym synem staje sie");
    temp->setLeft(node);
    print(temp, temp2, "ojcem staje sie");
    temp->setParent(temp2);
    print(node, temp, "ojcem staje sie");
    node->setParent(temp);

    if(temp2 != NULL){
        if(temp2->getLeft() == node){
            print(temp2, temp, "lewym synem staje sie");
            temp2->setLeft(temp);
        }else{
            print(temp2, temp, "prawym synem staje sie");
            temp2->setRight(temp);
        }
    }else{
        print(getRoot(), temp, "staje sie");
        this->root = temp;
    }

    if(temp->getBalanceFactor() == -1){
        node->setBalanceFactor(0);
        temp->setBalanceFactor(0);
    }else{
        node->setBalanceFactor(-1);
        temp->setBalanceFactor(1);
    }
}

void AVLTree::rotationLeft(BinaryTreeNode* node){

    BinaryTreeNode* temp = node->getLeft();
    BinaryTreeNode* temp2 = node->getParent();

    print(node, temp->getRight(), "lewym synem staje sie");
    node->setLeft(temp->getRight());

    if(node->getLeft() != NULL){
        node->getLeft()->setParent(node);
    }

    print(temp, node, "prawym synem staje sie");
    temp->setRight(node);
    print(temp, temp2, "ojcem staje sie");
    temp->setParent(temp2);
    print(node, temp, "ojcem staje sie");
    node->setParent(temp);

    if(temp2 != NULL){
        if(temp2->getLeft() == node){
            print(temp2, temp, "lewym synem staje sie");
            temp2->setLeft(temp);
        }else{
            print(temp2, temp, "prawym synem staje sie");
            temp2->setRight(temp);
        }
    }else{
        print(getRoot(), temp, "staje sie");
        this->root = temp;
    }

    if(temp->getBalanceFactor() == 1){
        node->setBalanceFactor(0);
        temp->setBalanceFactor(0);
    }else{
        node->setBalanceFactor(1);
        temp->setBalanceFactor(-1);
    }
}

void AVLTree::rotationRightLeft(BinaryTreeNode* node){
    BinaryTreeNode* temp = node->getRight();
    BinaryTreeNode* temp2 = node->getParent();
    BinaryTreeNode* temp3 = node->getRight()->getLeft();

    print(temp, temp3, "lewym synem staje sie");
    temp->setLeft(temp3->getRight());

    if(temp->getLeft() != NULL){
        print(temp->getLeft(), temp, "ojcem staje sie");
        temp->getLeft()->setParent(temp);
    }

    print(node, temp3->getLeft(), "prawym synem staje sie");
    node->setRight(temp3->getLeft());
    if(node->getRight() != NULL){
        print(node->getRight(), node, "ojcem staje sie");
        node->getRight()->setParent(node);
    }

    print(temp3, node, "lewym synem staje sie");
    temp3->setLeft(node);
    print(temp3, temp, "prawym synem staje sie");
    temp3->setRight(temp);
    print(node, temp3, "ojcem staje sie");
    node->setParent(temp3);
    print(temp, temp3, "ojcem staje sie");
    temp->setParent(temp3);
    print(temp3, temp2, "ojcem staje sie");
    temp3->setParent(temp2);

    if(temp2 != NULL){
        if(temp2->getLeft() == node){
            print(temp2, temp3, "lewym synem staje sie");
            temp2->setLeft(temp3);
        }else{
            print(temp2, temp3, "prawym synem staje sie");
            temp2->setRight(temp3);
        }
    }else{
        print(getRoot(), temp3, "staje sie");
        this->root = temp3;
    }


    if(temp3->getBalanceFactor() == -1){
        node->setBalanceFactor(1);
    }else{
        node->setBalanceFactor(0);
    }

    if(temp3->getBalanceFactor() == 1){
        temp->setBalanceFactor(-1);
    }else{
        temp->setBalanceFactor(0);
    }

    temp3->setBalanceFactor(0);

}

void AVLTree::rotationLeftRight(BinaryTreeNode* node){
    BinaryTreeNode* temp = node->getLeft();
    BinaryTreeNode* temp2 = node->getParent();
    BinaryTreeNode* temp3 = node->getLeft()->getRight();

    print(temp, temp3->getLeft(), "prawym synem staje sie");
    temp->setRight(temp3->getLeft());
    if(temp->getRight() != NULL){
        print(temp->getRight(), temp, "ojcem staje sie");
        temp->getRight()->setParent(temp);
    }

    print(node, temp3->getRight(), "lewym synem staje sie");
    node->setLeft(temp3->getRight());
    if(node->getLeft() != NULL){
        print(node->getLeft(), node, "ojcem staje sie");
        node->getLeft()->setParent(node);
    }

    print(temp3, node, "prawym synem staje sie");
    temp3->setRight(node);
    print(temp3, temp, "lewym synem staje sie");
    temp3->setLeft(temp);
    print(node, temp3, "ojcem staje sie");
    node->setParent(temp3);
    print(temp, temp3, "ojcem staje sie");
    temp->setParent(temp3);
    print(temp3, temp2, "ojcem staje sie");
    temp3->setParent(temp2);


    if(temp2 != NULL){
        if(temp2->getLeft() == node){
            print(temp2, temp3, "lewym synem staje sie");
            temp2->setLeft(temp3);
        }else{
            print(temp2, temp3, "prawym synem staje sie");
            temp2->setRight(temp3);
        }
    }else{
        print(getRoot(), temp3, "staje sie");
        this->root = temp3;
    }

    if(temp3->getBalanceFactor() == 1){
        node->setBalanceFactor(-1);
    }else{
        node->setBalanceFactor(0);
    }

    if(temp3->getBalanceFactor() == -1){
        temp->setBalanceFactor(1);
    }else{
        temp->setBalanceFactor(0);
    }

    temp3->setBalanceFactor(0);

}
